#!/bin/bash

IMAGE="debian:debian-10:10:0.20200511.260"
NETCFG="/etc/network/interfaces.d/50-cloud-init"
KNOWN_HOSTS="/home/trstringer/.ssh/known_hosts"

OUTPUT=$(az_vm_create "$IMAGE" "$IMAGE validation")
VM_NAME=$(echo "$OUTPUT" | head -n 1 | awk '{print $3}')
