#!/bin/bash

command -v jq > /dev/null 2>&1 || {
    echo "jq not found on PATH" >&2
    exit 1
}

# setable parameters:
IMAGE="debian-10-amd64-20200610-293"
IMAGE_OWNER=136693071363
LAUNCH_TEMPLATE_NAME=cloud-init-bpo-test
typeset -A VM_PUBLIC_IPS

az_vm_create() {
    local image tag ami instance_date instance_id
    image="$1"; shift
    tag="$1"; shift
    ami=$(aws ec2 describe-images --owners ${IMAGE_OWNER} \
	      --filters=Name=name,Values=${IMAGE} --output json | jq -r .Images[0].ImageId)
    test -n "$ami" || {
	echo "Unable to identify AMI ID for $IMAGE_OWNER/$IMAGE" >&2
	exit 1
    }
    instance_id=$(aws ec2 run-instances --image-id $ami --output json \
		      --launch-template LaunchTemplateName="$LAUNCH_TEMPLATE_NAME" \
		      --tag-specifications ResourceType=instance,Tags=\{Key=Name,Value="${tag}"\} | \
		      jq -r '.Instances[0].InstanceId')
    test -n "$instance_id" || {
	echo "FATAL: run-instances did not return an instance ID" >&2
	exit 1
    }
    aws ec2 wait instance-running --instance-ids "$instance_id" >&2
    aws ec2 describe-instances --output json --instance-id "$instance_id" | jq '.Reservations[0].Instances[0]'
}

az_vm_ssh() {
    local instance=$1; shift
    local ip=${VM_PUBLIC_IPS[$instance]}
    ssh -xak -o BatchMode=yes -o LogLevel=ERROR \
	-o UserKnownHostsFile=/dev/null \
	-o StrictHostKeyChecking=no \
	-l admin "$ip" "$@"
}

ec2_set_imdsv2_only() {
    local instance_id="$1"; shift
    aws ec2 modify-instance-metadata-options --instance-id "$instance_id" --http-tokens required
}

ec2_enable_imdsv1() {
    local instance_id="$1"; shift
    aws ec2 modify-instance-metadata-options --instance-id "$instance_id" --http-tokens optional
}

ec2_sru_validation() {
    local VM_NAME=$1 ; shift

    echo "$(date) Begin SRU validation"

    az_vm_ssh "$VM_NAME" "cloud-init status --wait --long"
    if az_vm_ssh "$VM_NAME" 'grep Trace /var/log/cloud-init.log'; then
	echo "Potential issue in cloud-init logs"
	exit 1
    fi
    az_vm_ssh "$VM_NAME" "sudo cat /run/cloud-init/result.json"
    az_vm_ssh "$VM_NAME" "sudo systemd-analyze blame"
    az_vm_ssh "$VM_NAME" "cloud-init analyze show"
    az_vm_ssh "$VM_NAME" "cloud-init analyze blame"
    echo 'After upgrade Networking config'
    az_vm_ssh "$VM_NAME" "cloud-init query --format \"'cloud-region: {{cloud_name}}-{{region}}'\""
    echo 'Get cloud-id'
    az_vm_ssh "$VM_NAME" "cloud-id"
    az_vm_ssh "$VM_NAME" "cloud-init query --format \"'cloud-region: {{cloud_name}}-{{ds.meta_data.placement.availability_zone}}'\""
    echo 'Validating whether metadata is being updated per boot LP:1819913. Expect last log to be Sytem Boot'
    az_vm_ssh "$VM_NAME" "sudo reboot" || true
    sleep 30
    az_vm_ssh "$VM_NAME" "cloud-init status --wait --long"
    echo 'After reboot'
    az_vm_ssh "$VM_NAME" "grep 'Update datasource' /var/log/cloud-init.log"

    echo "$(date) - Succeeded SRU validation"
}

ec2_imdsv2_validation() {
    local VM_NAME=$1 ; shift
    echo "$(date) Begin EC2 IMDSv2 validation"
    ec2_set_imdsv2_only "$VM_NAME"
    az_vm_ssh "$VM_NAME" "sudo cloud-init clean --logs --reboot" || true

    sleep 60

    az_vm_ssh "$VM_NAME" "cloud-init status --wait --long"
    az_vm_ssh "$VM_NAME" "sudo cat /run/cloud-init/result.json"
    az_vm_ssh "$VM_NAME" "cloud-init query --format \"'cloud-region: {{cloud_name}}-{{ds.meta_data.placement.availability_zone}}'\""
    az_vm_ssh "$VM_NAME" "grep 'Fetching Ec2 IMDSv2 API Token' /var/log/cloud-init.log"
    ec2_enable_imdsv1 "$VM_NAME"
}

KNOWN_HOSTS="/home/trstringer/.ssh/known_hosts"

