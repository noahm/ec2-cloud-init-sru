#!/bin/bash -i
# This script launches a standard buster AMI and performs testing:
#  - Cloud-init's current state is displayed
#  - Cloud-init is updated to the buster bpo version
#  - The cloud-init SRU testing is performed

set -e

test_platform="$1"; shift
if [ -z "$test_platform" ] ||
       [ ! -f "platform/${test_platform}.sh" ]; then
    usage
    exit 1
fi

. "platform/${test_platform}.sh"

OUTPUT=$(az_vm_create "$IMAGE" "$IMAGE validation")
VM_NAME=$(echo "$OUTPUT" | jq -r '.InstanceId')
VM_PUBLIC_IPS[$VM_NAME]=$(echo "$OUTPUT" | jq -r '.NetworkInterfaces[0].Association.PublicIp')

echo "$(date) - Created $VM_NAME"

now=$(date +%s)
deadline=$((now+300))
while ! az_vm_ssh "$VM_NAME" 'exit 0'; do
    echo "Waiting for instance ssh to respond"
    sleep 1
    if [ $(date +%s) -gt $deadline ]; then
	echo "Deadline exceeded. Giving up."
	exit 1
    fi
done

az_vm_ssh \
    "$VM_NAME" \
    'echo "deb http://deb.debian.org/debian/ buster-backports main contrib non-free" | sudo tee /etc/apt/sources.list.d/backports.list'

APT_PREFS="Package: *
Pin: release a=stable
Pin-Priority: 500

Package: *
Pin: release a=buster-backports
Pin-Priority: 400"
az_vm_ssh "$VM_NAME" "echo '$APT_PREFS' | sudo tee /etc/apt/preferences.d/backports.pref"

az_vm_ssh "$VM_NAME" "sudo apt-get update"

az_vm_ssh "$VM_NAME" "sudo DEBIAN_FRONTEND=noninteractive apt-get install -y -t buster-backports cloud-init=20.2-2~bpo10+1"

az_vm_ssh "$VM_NAME" "sudo cloud-init clean --logs --reboot" || true

sleep 60

az_vm_ssh "$VM_NAME" "cloud-init status --long"
az_vm_ssh "$VM_NAME" "dpkg-query --show cloud-init"
az_vm_ssh "$VM_NAME" "sudo cat /run/cloud-init/result.json"
if az_vm_ssh "$VM_NAME" "grep Trace /var/log/cloud-init.log"; then
    echo "Potential issue in cloud-init.log"
    exit 1
fi
az_vm_ssh "$VM_NAME" "systemd-analyze"
az_vm_ssh "$VM_NAME" "systemd-analyze blame"
az_vm_ssh "$VM_NAME" "cloud-init analyze show"
az_vm_ssh "$VM_NAME" "cloud-init analyze blame"

az_vm_ssh "$VM_NAME" "dpkg-query --show cloud-init"
az_vm_ssh "$VM_NAME" "sudo hostname SRU-didnt-work"
az_vm_ssh "$VM_NAME" "sudo cloud-init clean --logs --reboot" || true

sleep 60

ec2_sru_validation "$VM_NAME"
ec2_imdsv2_validation "$VM_NAME"
