# cloud-init upgrade validation #

Validating proposed updates of cloud-init for inclusion in Debian stable point releases.

These scripts are inspired by [scripts published by Thomas Stringer](https://salsa.debian.org/trstringer/azure-debian/-/blob/master/buster/sru_validate_debian_debian-10_10_0.20200511.260_with_cloud-init_20.2-2_bp.sh)

The scripts have the ability to test in-place upgrades of cloud-init,
as well as images prebuilt with the proposed cloud-init.  They're
still pretty rudimentary, though, so they very well may not work for
you.

The EC2 IMDSv2 test requires that you have a more modern version of
the awscli package than the one that ships with buster.

Results are in the [test-results/](test-results/) subdirectory.

