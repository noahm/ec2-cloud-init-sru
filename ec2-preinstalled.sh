#!/bin/bash -i
# This script performs the cloud-init SRU testing on a an instance
# launched from buster AMI with the buster-backports cloud-init
# pre-installed.

set -e

test_platform="$1"; shift
if [ -z "$test_platform" ] ||
       [ ! -f "platform/${test_platform}.sh" ]; then
    usage
    exit 1
fi

. "platform/${test_platform}.sh"

# Override the image and launch template details:
IMAGE="scratch-nmeyerha-buster-bpo-hvm-x86_64-gp2-2020-06-17-61899"
IMAGE_OWNER=938878489526
LAUNCH_TEMPLATE_NAME=cloud-init-bpo-test

OUTPUT=$(az_vm_create "$IMAGE" "$IMAGE validation")
VM_NAME=$(echo "$OUTPUT" | jq -r '.InstanceId')
VM_PUBLIC_IPS[$VM_NAME]=$(echo "$OUTPUT" | jq -r '.NetworkInterfaces[0].Association.PublicIp')

echo "$(date) - Created $VM_NAME"

now=$(date +%s)
deadline=$((now+300))
while ! az_vm_ssh "$VM_NAME" 'exit 0'; do
    echo "Waiting for instance ssh to respond"
    sleep 1
    if [ $(date +%s) -gt $deadline ]; then
	echo "Deadline exceeded. Giving up."
	exit 1
    fi
done

ec2_sru_validation "$VM_NAME"
ec2_imdsv2_validation "$VM_NAME"
